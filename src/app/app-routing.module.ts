import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddcarsComponent } from './addcars/addcars.component';
import { AddintermedaitorComponent } from './addintermedaitor/addintermedaitor.component';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { NavigationComponent } from './user-pages/navigation/navigation.component';
import { HomeComponent } from './user-pages/home/home.component';
import { ForgotPasswordComponent } from './user-pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './user-pages/reset-password/reset-password.component';
import { AddbuyerComponent } from './user-modules/addbuyer/addbuyer.component';
import { ContactComponent } from './user-pages/contact/contact.component';
import { PricingComponent } from './user-pages/pricing/pricing.component';
import { AddcityComponent } from './admin-modules/app-features/addcity/addcity.component';
import { AddcountryComponent } from './admin-modules/app-features/addcountry/addcountry.component';
import { AddcurrencyComponent } from './admin-modules/app-features/addcurrency/addcurrency.component';
import { AddpayoutScheduleComponent } from './admin-modules/app-features/addpayout-schedule/addpayout-schedule.component';
import { AddproductTypeComponent } from './admin-modules/app-features/addproduct-type/addproduct-type.component';
import { AddreasonComponent } from './admin-modules/app-features/addreason/addreason.component';
import { AddroleComponent } from './admin-modules/app-features/addrole/addrole.component';
import { AddstateComponent } from './admin-modules/app-features/addstate/addstate.component';
import { AddsubscriptionPlanComponent } from './admin-modules/app-features/addsubscription-plan/addsubscription-plan.component';
import { AddtradingTypeComponent } from './admin-modules/app-features/addtrading-type/addtrading-type.component';
import { AdduserTypeComponent } from './admin-modules/app-features/adduser-type/adduser-type.component';
import { CmsInfoComponent } from './admin-modules/cms/cms-info/cms-info.component';
import { CmsFeaturesComponent } from './admin-modules/cms/cms-features/cms-features.component';
import { CmsBannerComponent } from './admin-modules/cms/cms-banner/cms-banner.component';
import { CmsServicesComponent } from './admin-modules/cms/cms-services/cms-services.component';
import { CmsHeroSectionComponent } from './admin-modules/cms/cms-hero-section/cms-hero-section.component';
import { AddsellerComponent } from './user-modules/addseller/addseller.component';
import { CmsBuyerSellerComponent } from './admin-modules/cms/cms-buyer-seller/cms-buyer-seller.component';
import { BuyerMainLayoutComponent } from './buyer-modules/layouts/buyer-main-layout/buyer-main-layout.component';
import { SellerMainLayoutComponent } from './seller-modules/layouts/seller-main-layout/seller-main-layout.component';
import { BuyerDashboardComponent } from './buyer-modules/layouts/buyer-dashboard/buyer-dashboard.component';
import { SellerDashboardComponent } from './seller-modules/layouts/seller-dashboard/seller-dashboard.component';
import { ListOrdersComponent } from './seller-modules/order-management/list-orders/list-orders.component';
import { OrderManagementComponent } from './seller-modules/order-management/order-management.component';
import { ViewOrdersComponent } from './seller-modules/order-management/view-orders/view-orders.component';
import { ProductManagementComponent } from './seller-modules/product-management/product-management.component';
import { ListProductsComponent } from './seller-modules/product-management/list-products/list-products.component';
import { ViewProductComponent } from './seller-modules/product-management/view-product/view-product.component';
import { TeamManagementComponent } from './seller-modules/team-management/team-management.component';
import { ListTeamsComponent } from './seller-modules/team-management/list-teams/list-teams.component';
import { ViewTeamComponent } from './seller-modules/team-management/view-team/view-team.component';
import { BuyerOrderManagementComponent } from './buyer-modules/buyer-order-management/buyer-order-management.component';
import { BuyerListOrdersComponent } from './buyer-modules/buyer-order-management/buyer-list-orders/buyer-list-orders.component';
import { BuyerViewOrderComponent } from './buyer-modules/buyer-order-management/buyer-view-order/buyer-view-order.component';
import { AdminOrderManagementComponent } from './admin-modules/admin-order-management/admin-order-management.component';
import { AdminListOrdersComponent } from './admin-modules/admin-order-management/admin-list-orders/admin-list-orders.component';
import { AdminViewOrderDetailsComponent } from './admin-modules/admin-order-management/admin-view-order-details/admin-view-order-details.component';
import { AdminAddBannerComponent } from './admin-modules/cms/cms-banner/admin-add-banner/admin-add-banner.component';
import { AdminAddFeaturesComponent } from './admin-modules/cms/cms-features/admin-add-features/admin-add-features.component';
import { AdminAddServicesComponent } from './admin-modules/cms/cms-services/admin-add-services/admin-add-services.component';
import { AdminAddbuyerComponent } from './user-modules/addbuyer/admin-addbuyer/admin-addbuyer.component';
import { AdminAddsellerComponent } from './user-modules/addseller/admin-addseller/admin-addseller.component';
import { AdminAddintermediatorComponent } from './user-modules/addintermediator/admin-addintermediator/admin-addintermediator.component';
import { AdminGroupManagementComponent } from './admin-modules/admin-parts-management/admin-group-management/admin-group-management.component';
import { AdminSubGroupManagementComponent } from './admin-modules/admin-parts-management/admin-sub-group-management/admin-sub-group-management.component';
import { AdminSubNodeManagementComponent } from './admin-modules/admin-parts-management/admin-sub-node-management/admin-sub-node-management.component';
import { AdminParameterManagementComponent } from './admin-modules/admin-parts-management/admin-parameter-management/admin-parameter-management.component';
import { AdminBrandManagementComponent } from './admin-modules/admin-parts-management/admin-brand-management/admin-brand-management.component';
import { AdminCarManagementComponent } from './admin-modules/admin-parts-management/admin-car-management/admin-car-management.component';
import { AdminModelManagementComponent } from './admin-modules/admin-parts-management/admin-model-management/admin-model-management.component';
import { CmsSocialMediaLinksComponent } from './admin-modules/cms/cms-social-media-links/cms-social-media-links.component';
import { AdminAddSocialMediaLinksComponent } from './admin-modules/cms/cms-social-media-links/admin-add-social-media-links/admin-add-social-media-links.component';
import { CmsUsefullLinksComponent } from './admin-modules/cms/cms-usefull-links/cms-usefull-links.component';
import { AdminAddUsefullLinksComponent } from './admin-modules/cms/cms-usefull-links/admin-add-usefull-links/admin-add-usefull-links.component';
import { CmsNavLinksComponent } from './admin-modules/cms/cms-nav-links/cms-nav-links.component';
import { AdminAddNavLinksComponent } from './admin-modules/cms/cms-nav-links/admin-add-nav-links/admin-add-nav-links.component';
import { AdminCategoryRequestStatusComponent } from './admin-modules/admin-status-management/admin-category-request-status/admin-category-request-status.component';
import { AdminMakeOfferStatusComponent } from './admin-modules/admin-status-management/admin-make-offer-status/admin-make-offer-status.component';
import { AdminOrderStatusComponent } from './admin-modules/admin-status-management/admin-order-status/admin-order-status.component';
import { AdminPartsRequestStatusComponent } from './admin-modules/admin-status-management/admin-parts-request-status/admin-parts-request-status.component';
import { AdminPaymentStatusComponent } from './admin-modules/admin-status-management/admin-payment-status/admin-payment-status.component';
import { AdminQuoteStatusComponent } from './admin-modules/admin-status-management/admin-quote-status/admin-quote-status.component';
import { AdminRegistrationStatusComponent } from './admin-modules/admin-status-management/admin-registration-status/admin-registration-status.component';
import { AdminShipmentStatusComponent } from './admin-modules/admin-status-management/admin-shipment-status/admin-shipment-status.component';
import { AddAdminCategoryRequestStatusComponent } from './admin-modules/admin-status-management/admin-category-request-status/add-admin-category-request-status/add-admin-category-request-status.component';
import { AdminTaxCodesComponent } from './admin-modules/admin-tax-management/admin-tax-codes/admin-tax-codes.component';
import { AddAdminTaxCodesComponent } from './admin-modules/admin-tax-management/admin-tax-codes/add-admin-tax-codes/add-admin-tax-codes.component';
import { AdminJurisdictionComponent } from './admin-modules/admin-tax-management/admin-jurisdiction/admin-jurisdiction.component';
import { AddAdminJurisdictionComponent } from './admin-modules/admin-tax-management/admin-jurisdiction/add-admin-jurisdiction/add-admin-jurisdiction.component';
import { AdminPlaceOfSupplyComponent } from './admin-modules/admin-tax-management/admin-place-of-supply/admin-place-of-supply.component';
import { AddAdminPlaceOfSupplyComponent } from './admin-modules/admin-tax-management/admin-place-of-supply/add-admin-place-of-supply/add-admin-place-of-supply.component';
import { AdminVatDetailsComponent } from './admin-modules/admin-tax-management/admin-vat-details/admin-vat-details.component';
import { AddAdminVatDetailsComponent } from './admin-modules/admin-tax-management/admin-vat-details/add-admin-vat-details/add-admin-vat-details.component';
import { AddAdminMakeOfferStatusComponent } from './admin-modules/admin-status-management/admin-make-offer-status/add-admin-make-offer-status/add-admin-make-offer-status.component';
import { AddAdminOrderStatusComponent } from './admin-modules/admin-status-management/admin-order-status/add-admin-order-status/add-admin-order-status.component';
import { AddAdminPartsRequestStatusComponent } from './admin-modules/admin-status-management/admin-parts-request-status/add-admin-parts-request-status/add-admin-parts-request-status.component';
import { AddAdminPaymentStatusComponent } from './admin-modules/admin-status-management/admin-payment-status/add-admin-payment-status/add-admin-payment-status.component';
import { AddAdminQuoteStatusComponent } from './admin-modules/admin-status-management/admin-quote-status/add-admin-quote-status/add-admin-quote-status.component';
import { AddAdminRegistrationStatusComponent } from './admin-modules/admin-status-management/admin-registration-status/add-admin-registration-status/add-admin-registration-status.component';
import { AddAdminShipmentStatusComponent } from './admin-modules/admin-status-management/admin-shipment-status/add-admin-shipment-status/add-admin-shipment-status.component';
import { AdminAddCountryComponent } from './admin-modules/app-features/addcountry/admin-add-country/admin-add-country.component';
import { AdminAddCurrencyComponent } from './admin-modules/app-features/addcurrency/admin-add-currency/admin-add-currency.component';
import { AdminAddPayoutScheduleComponent } from './admin-modules/app-features/addpayout-schedule/admin-add-payout-schedule/admin-add-payout-schedule.component';
import { AdminAddProductTypeComponent } from './admin-modules/app-features/addproduct-type/admin-add-product-type/admin-add-product-type.component';
import { AdminAddReasonComponent } from './admin-modules/app-features/addreason/admin-add-reason/admin-add-reason.component';
import { AdminAddRoleComponent } from './admin-modules/app-features/addrole/admin-add-role/admin-add-role.component';
import { AdminAddStateComponent } from './admin-modules/app-features/addstate/admin-add-state/admin-add-state.component';
import { AdminAddSubscriptionPlanComponent } from './admin-modules/app-features/addsubscription-plan/admin-add-subscription-plan/admin-add-subscription-plan.component';
import { AdminAddTradingTypeComponent } from './admin-modules/app-features/addtrading-type/admin-add-trading-type/admin-add-trading-type.component';
import { AdminAddUserTypeComponent } from './admin-modules/app-features/adduser-type/admin-add-user-type/admin-add-user-type.component';
import { BuyerNavigationComponent } from './buyer-modules/layouts/buyer-pages/buyer-navigation/buyer-navigation.component';
import { BuyerHomeComponent } from './buyer-modules/layouts/buyer-pages/buyer-home/buyer-home.component';
import { BuyerPagesComponent } from './buyer-modules/layouts/buyer-pages/buyer-pages.component';
import { AdminSmsTemplateManagementComponent } from './admin-modules/admin-templates-management/admin-sms-template-management/admin-sms-template-management.component';
import { AdminEmailTemplateManagementComponent } from './admin-modules/admin-templates-management/admin-email-template-management/admin-email-template-management.component';
import { AdminAddSmsTemplatesComponent } from './admin-modules/admin-templates-management/admin-sms-template-management/admin-add-sms-templates/admin-add-sms-templates.component';
import { AdminAddEmailTemplatesComponent } from './admin-modules/admin-templates-management/admin-email-template-management/admin-add-email-templates/admin-add-email-templates.component';
import { AdminSubscriptionManagementComponent } from './admin-modules/admin-subscription-management/admin-subscription-management.component';
import { AdminAddSubscriptionManagementComponent } from './admin-modules/admin-subscription-management/admin-add-subscription-management/admin-add-subscription-management.component';
import { AdminProductManagementComponent } from './admin-modules/admin-product-management/admin-product-management.component';
import { AdminAddProductManagementComponent } from './admin-modules/admin-product-management/admin-add-product-management/admin-add-product-management.component';
import { AdminCommissionManagementComponent } from './admin-modules/admin-commission-management/admin-commission-management.component';
import { AdminAddCommissionManagementComponent } from './admin-modules/admin-commission-management/admin-add-commission-management/admin-add-commission-management.component';
import { AdminVinSearchHistoryManagementComponent } from './admin-modules/admin-vin-search-history-management/admin-vin-search-history-management.component';
import { AdminAddOrderComponent } from './admin-modules/admin-order-management/admin-add-order/admin-add-order.component';
import { AdminStaticPageManagementComponent } from './admin-modules/admin-static-page-management/admin-static-page-management.component';
import { AdminAddStaticPageComponent } from './admin-modules/admin-static-page-management/admin-add-static-page/admin-add-static-page.component';
import { AdminMakeOfferManagementComponent } from './admin-modules/admin-make-offer-management/admin-make-offer-management.component';
import { AdminAddMakeOfferComponent } from './admin-modules/admin-make-offer-management/admin-add-make-offer/admin-add-make-offer.component';
import { AdminPaymentManagementComponent } from './admin-modules/admin-payment-management/admin-payment-management.component';
import { AdminAddPaymentComponent } from './admin-modules/admin-payment-management/admin-add-payment/admin-add-payment.component';
import { AdminConfigurationDetailsManagementComponent } from './admin-modules/admin-templates-management/admin-configuration-details-management/admin-configuration-details-management.component';
import { AdminAddConfigurationDetailsComponent } from './admin-modules/admin-templates-management/admin-configuration-details-management/admin-add-configuration-details/admin-add-configuration-details.component';
import { CmsMobileSectionComponent } from './admin-modules/cms/cms-mobile-section/cms-mobile-section.component';
import { AdminAddMobileSectionComponent } from './admin-modules/cms/cms-mobile-section/admin-add-mobile-section/admin-add-mobile-section.component';


const routes: Routes = [
  {path:'',redirectTo:'navigation',pathMatch:'full'},
  {path:'navigation',component:NavigationComponent, children:[
    {path:'',redirectTo:'home',pathMatch:'full'},
    {path:'home',component:HomeComponent},
    {path:'contact',component:ContactComponent},
    {path:'pricing',component:PricingComponent},
  ]},
  {path:'home',component:LandingComponent},
  {path:'login',component:LoginComponent},
  {path:'forgot-password',component:ForgotPasswordComponent},
  {path:'reset-password',component:ResetPasswordComponent},
  
  {path:'admin',component:AdminComponent,children:[
    {path:'',redirectTo:'dashboard',pathMatch:'full'},
    {path:'dashboard', component:DashboardComponent},
    {path:'buyermanagement', component:AddbuyerComponent},
    {path:'buyer/:type/:id', component:AdminAddbuyerComponent},
    {path:'sellermanagement', component:AddsellerComponent},
    {path:'seller/:type/:id', component:AdminAddsellerComponent},
    {path:'intermedaitormanagement', component:AddintermedaitorComponent},
    {path:'intermediator/:type/:id', component:AdminAddintermediatorComponent},
    {path:'cars', component:AddcarsComponent},
    {path:'parts-management/group-management', component:AdminGroupManagementComponent},
    {path:'parts-management/sub-group-management', component:AdminSubGroupManagementComponent},
    {path:'parts-management/sub-node-management', component:AdminSubNodeManagementComponent},
    {path:'parts-management/parameters-management', component:AdminParameterManagementComponent},
    {path:'parts-management/brand-management', component:AdminBrandManagementComponent},
    {path:'parts-management/car-management', component:AdminCarManagementComponent},
    {path:'parts-management/model-management', component:AdminModelManagementComponent},
    {path:'status-management/category-request', component:AdminCategoryRequestStatusComponent},
    {path:'status-management/category-request/:type/:id', component:AddAdminCategoryRequestStatusComponent},
    {path:'status-management/make-offer', component:AdminMakeOfferStatusComponent},
    {path:'status-management/make-offer/:type/:id', component:AddAdminMakeOfferStatusComponent},
    {path:'status-management/order', component:AdminOrderStatusComponent},
    {path:'status-management/order/:type/:id', component:AddAdminOrderStatusComponent},
    {path:'status-management/parts-request', component:AdminPartsRequestStatusComponent},
    {path:'status-management/parts-request/:type/:id', component:AddAdminPartsRequestStatusComponent},
    {path:'status-management/payment', component:AdminPaymentStatusComponent},
    {path:'status-management/payment/:type/:id', component:AddAdminPaymentStatusComponent},
    {path:'status-management/quote', component:AdminQuoteStatusComponent},
    {path:'status-management/quote/:type/:id', component:AddAdminQuoteStatusComponent},
    {path:'status-management/registration', component:AdminRegistrationStatusComponent},
    {path:'status-management/registration/:type/:id', component:AddAdminRegistrationStatusComponent},
    {path:'status-management/shipment', component:AdminShipmentStatusComponent},
    {path:'status-management/shipment/:type/:id', component:AddAdminShipmentStatusComponent},
    {path:'tax-management/tax-codes', component:AdminTaxCodesComponent},
    {path:'tax-management/tax-codes/:type/:id', component:AddAdminTaxCodesComponent},
    {path:'tax-management/jurisdiction', component:AdminJurisdictionComponent},
    {path:'tax-management/jurisdiction/:type/:id', component:AddAdminJurisdictionComponent},
    {path:'tax-management/place-of-supply', component:AdminPlaceOfSupplyComponent},
    {path:'tax-management/place-of-supply/:type/:id', component:AddAdminPlaceOfSupplyComponent},
    {path:'tax-management/vat-details', component:AdminVatDetailsComponent},
    {path:'tax-management/vat-details/:type/:id', component:AddAdminVatDetailsComponent},
    {path:'city-management', component:AddcityComponent},
    
    {path:'country-management', component:AddcountryComponent},
    {path:'country-management/configure/:type/:id', component:AdminAddCountryComponent},
    {path:'currency-management', component:AddcurrencyComponent},
    {path:'currency-management/configure/:type/:id', component:AdminAddCurrencyComponent},
    {path:'payout-schedule-management', component:AddpayoutScheduleComponent},
    {path:'payout-schedule-management/configure/:type/:id', component:AdminAddPayoutScheduleComponent},
    {path:'product-type-management', component:AddproductTypeComponent},
    {path:'product-type-management/configure/:type/:id', component:AdminAddProductTypeComponent},
    {path:'reason-management', component:AddreasonComponent},
    {path:'reason-management/configure/:type/:id', component:AdminAddReasonComponent},
    {path:'role-management', component:AddroleComponent},
    {path:'role-management/configure/:type/:id', component:AdminAddRoleComponent},
    {path:'state-management', component:AddstateComponent},
    {path:'state-management/configure/:type/:id', component:AdminAddStateComponent},
    {path:'subscription-plan-management', component:AddsubscriptionPlanComponent},
    {path:'subscription-plan-management/configure/:type/:id', component:AdminAddSubscriptionPlanComponent},
    {path:'trading-type-management', component:AddtradingTypeComponent},
    {path:'trading-type-management/configure/:type/:id', component:AdminAddTradingTypeComponent},
    {path:'user-type-management', component:AdduserTypeComponent},
    {path:'user-type-management/configure/:type/:id', component:AdminAddUserTypeComponent},
    {path:'cms-info', component:CmsInfoComponent},
    {path:'cms-features', component:CmsFeaturesComponent},
    {path:'cms/features/:type/:id', component:AdminAddFeaturesComponent},
    {path:'cms-banner', component:CmsBannerComponent},
    {path:'cms/banner/:type/:id', component:AdminAddBannerComponent},
    {path:'cms-services', component:CmsServicesComponent},
    {path:'cms/services/:type/:id', component:AdminAddServicesComponent},
    {path:'cms-about', component:CmsHeroSectionComponent},
    {path:'cms-social-media-links', component:CmsSocialMediaLinksComponent},
    {path:'cms/social-media-links/:type/:id', component:AdminAddSocialMediaLinksComponent},
    {path:'cms-usefull-links', component:CmsUsefullLinksComponent},
    {path:'cms/usefull-links/:type/:id', component:AdminAddUsefullLinksComponent},
    {path:'cms-nav-links', component:CmsNavLinksComponent},
    {path:'cms/nav-links/:type/:id', component:AdminAddNavLinksComponent},
    {path:'cms-mobile-section', component:CmsMobileSectionComponent},
    {path:'cms/mobile-section/:type/:id', component:AdminAddMobileSectionComponent},
    {path:'cms-buyer-seller', component:CmsBuyerSellerComponent},
    {path:'payment-management', component:AdminPaymentManagementComponent},
    {path:'payment-management/configure/:type/:id', component:AdminAddPaymentComponent},
    {path:'commission-management', component:AdminCommissionManagementComponent},
    {path:'commission-management/configure/:type/:id', component:AdminAddCommissionManagementComponent},
    {path:'product-management', component:AdminProductManagementComponent},
    {path:'product-management/configure/:type/:id', component:AdminAddProductManagementComponent},
    {path:'make-offer-management', component:AdminMakeOfferManagementComponent},
    {path:'make-offer-management/configure/:type/:id', component:AdminAddMakeOfferComponent},
    {path:'static-page-management', component:AdminStaticPageManagementComponent},
    {path:'static-page-management/configure/:type/:id', component:AdminAddStaticPageComponent},
    {path:'subscription-management', component:AdminSubscriptionManagementComponent},
    {path:'subscription-management/configure/:type/:id', component:AdminAddSubscriptionManagementComponent},
    {path:'templates-management/sms-templates-management', component:AdminSmsTemplateManagementComponent},
    {path:'templates-management/sms-templates-management/configure/:type/:id', component:AdminAddSmsTemplatesComponent},
    {path:'templates-management/email-templates-management', component:AdminEmailTemplateManagementComponent},
    {path:'templates-management/email-templates-management/configure/:type/:id', component:AdminAddEmailTemplatesComponent},
    {path:'vin-search-history-management', component:AdminVinSearchHistoryManagementComponent},
    {path:'templates-management/configuration-details-management/configure/:type/:id', component:AdminAddConfigurationDetailsComponent},
    {path:'templates-management/configuration-details-management', component:AdminConfigurationDetailsManagementComponent},
    {path:'order-management', component:AdminOrderManagementComponent, children:[
      {path:'',redirectTo:'list-orders',pathMatch:'full'},
      {path:'list-orders', component:AdminListOrdersComponent},
      {path:'add-order', component:AdminAddOrderComponent},
      {path:'view-order', component:AdminViewOrderDetailsComponent}
    ]}
  ]},
  {path:'buyer',component:BuyerPagesComponent,children:[
    {path:'navigation', component:BuyerNavigationComponent, children:[
      {path:'',redirectTo:'home',pathMatch:'full'},
      {path:'home',component:BuyerHomeComponent}
    ]},
  ]},
  {path:'buyers',component:BuyerMainLayoutComponent,children:[
    {path:'',redirectTo:'dashboard',pathMatch:'full'},
    {path:'dashboard', component:BuyerDashboardComponent},
    
    {path:'order-management', component:BuyerOrderManagementComponent, children:[
      {path:'',redirectTo:'list-orders',pathMatch:'full'},
      {path:'list-orders', component:BuyerListOrdersComponent},
      {path:'view-order', component:BuyerViewOrderComponent}
    ]}
  ]},
  {path:'sellers',component:SellerMainLayoutComponent,children:[
    {path:'',redirectTo:'dashboard',pathMatch:'full'},
    {path:'dashboard', component:SellerDashboardComponent},
    {path:'order-management', component:OrderManagementComponent, children:[
      {path:'',redirectTo:'list-orders',pathMatch:'full'},
      {path:'list-orders', component:ListOrdersComponent},
      {path:'view-order', component:ViewOrdersComponent}
    ]},
    {path:'product-management', component:ProductManagementComponent, children:[
      {path:'',redirectTo:'list-products',pathMatch:'full'},
      {path:'list-products', component:ListProductsComponent},
      {path:'view-product', component:ViewProductComponent}
    ]},
    {path:'team-management', component:TeamManagementComponent, children:[
      {path:'',redirectTo:'list-teams',pathMatch:'full'},
      {path:'list-teams', component:ListTeamsComponent},
      {path:'view-team', component:ViewTeamComponent}
    ]},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
