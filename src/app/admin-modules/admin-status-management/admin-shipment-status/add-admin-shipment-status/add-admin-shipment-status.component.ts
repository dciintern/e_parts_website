import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-add-admin-shipment-status',
  templateUrl: './add-admin-shipment-status.component.html',
  styleUrls: ['./add-admin-shipment-status.component.scss']
})
export class AddAdminShipmentStatusComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  ShipmentStatusList: any;
  dataLoaded: boolean;
  addmode: boolean;
  editmode: boolean;
  listmode: boolean;
  selectedData_ID: any;
  addShipmentStatusForm: FormGroup;
  deletemode: boolean;
  imageUploaded: boolean;
  ShipmentStatusUrl: string;
  type: string;
  id: any;

  constructor(private adminService:AdminModuleService, private router:Router, private _Activatedroute:ActivatedRoute,
              private toastr:ToastrManager, 
              private formBuilder:FormBuilder) {
                this.addShipmentStatusForm = this.formBuilder.group({
                  shipment_status_id:[''],
                  shipment_status_name:['',Validators.required],
                  remarks:['',Validators.required],
                  status:['true',Validators.required],
                  created_at:['',Validators.required],
                  created_by:['',Validators.required],
                  modified_at:['',Validators.required],
                  modified_by:['',Validators.required],
                });
               }

  ngOnInit(): void {

    this.dataLoaded = false;
    this.imageUploaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;

    this._Activatedroute.paramMap.subscribe(params => { 
      console.log(params);
      this.id = params.get('id'); 
      this.type = params.get('type'); 
      if (this.type == 'update') {
        this.dataLoaded = true;
        this.imageUploaded = false;
        this.addmode = false;
        this.editmode = true;
        this.deletemode = false;
        this.adminService.geShipmentStatusInfo(this.id).subscribe(data=>{
          if (data['success']) {
            this.addShipmentStatusForm.patchValue({
              shipment_status_id: data['data'].shipment_status_id,
              shipment_status_name: data['data'].shipment_status_name,
              remarks: data['data'].remarks,
              status: data['data'].status,
              created_at: data['data'].created_at,
              created_by: data['data'].created_by,
              modified_at: data['data'].modified_at,
              modified_by: data['data'].modified_by,
            });
            this.ShipmentStatusUrl = data['data'].icon;
            this.imageUploaded = true;
            $('.addBanner').modal('show');
            this.selectedData_ID =  data['data'].id;
            this.addmode = false;
            this.editmode = true;
            this.deletemode = false;
            this.listmode = false; 
          } else {
            
          }
        })
      } else {
        this.dataLoaded = true;
        this.imageUploaded = false;
        this.addmode = true;
        this.editmode = false;
        this.deletemode = false;
      }
    });

    

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };


  }

  getselected(event,i){
    console.log(i); 
  }

  getallselected(event){

  }

  addShipmentStatus(){
    this.adminService.createShipmentStatus(this.addShipmentStatusForm.value).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
      } else {
        this.showError(data['msg']);
      }
    })
  }



  updateShipmentStatus(){
    this.dataLoaded = false;
    this.adminService.updateShipmentStatus(this.addShipmentStatusForm.value).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showSuccess(data['msg']);
      }
    });
  }

  cancel(){
    $('#deleteModal').modal('hide');
    $('.addShipmentStatus').modal('hide');
    this.dataLoaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;
    this.imageUploaded = true;
    this.ShipmentStatusUrl = null;
    this.onPageReload();
    this.router.navigateByUrl('admin/status-management/shipment')
  }

  onPageReload(){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };
    this.adminService.getShipmentStatusList().subscribe(async data=>{
      if (data['success']) {
        this.ShipmentStatusList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.ShipmentStatusList = [];
      }
    });
  }


  showSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: 'Done',
      text: msg,
    })
  }

  showError(msg) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: msg,
    })
  }

  showWarning(msg) {
    Swal.fire({
      icon: 'warning',
      title: 'Check',
      text: msg,
    })
  }
}
