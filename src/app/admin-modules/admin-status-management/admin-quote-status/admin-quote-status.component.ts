import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-admin-quote-status',
  templateUrl: './admin-quote-status.component.html',
  styleUrls: ['./admin-quote-status.component.scss']
})
export class AdminQuoteStatusComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  QuotesStatusList: any;
  dataLoaded: boolean;
  addmode: boolean;
  editmode: boolean;
  listmode: boolean;
  selectedData_ID: any;
  addQuotesStatusForm: FormGroup;
  deletemode: boolean;
  imageUploaded: boolean;
  QuotesStatusUrl: string;
  filterMode: Boolean = true;
  advancefilterMode: Boolean = false;
  p: number = 1;
  itemPage: number = 10;
  FilteredState: boolean = false;
  advanceFilterForm:FormGroup;

  constructor(private adminService:AdminModuleService, private router:Router,
              private toastr:ToastrManager, 
              private formBuilder:FormBuilder) {
                this.addQuotesStatusForm = this.formBuilder.group({
                  id:[''],
                  title:['',Validators.required],
                  content:['',Validators.required],
                  icon:['',Validators.required],
                  status:['',Validators.required],
                  created_at:['',Validators.required],
                  created_by:['',Validators.required],
                  modified_at:['',Validators.required],
                  modified_by:['',Validators.required],
                });
               }

  ngOnInit(): void {

    this.dataLoaded = false;
    this.imageUploaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };


    this.adminService.getQuoteStatusList().subscribe(async data=>{
      if (data['success']) {
        this.QuotesStatusList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.QuotesStatusList = [];
      }
    });
  }

  openAdvanceFilter(){
    $('.bd-example-modal-lg').modal('show')
  }

  addAdvanceFilter(){
    
  }

  ClearFilter(){
    this.FilteredState = false;
  }

  FilterData(){
    this.FilteredState = true;
    $('.bd-example-modal-lg').modal('hide')
  }

  pageChanged(event:any){
    this.p = event;
  }

  getFilterState(val:any){
    if (val == 'filterMode') {
      this.filterMode = true;
      this.advancefilterMode = false;
    } else if (val == 'advancefilterMode'){
      this.filterMode = false;
      this.advancefilterMode = true;
      this.openAdvanceFilter();
    }
  }

  getselected(event,i){
    console.log(i); 
  }

  getallselected(event){

  }

  addQuotesStatus(){
    this.router.navigateByUrl('admin/status-management/quote/add/0');
  }


  validate(id){
    if (id != null) {
      $('#deleteModal').modal('show');
      this.selectedData_ID =  id;
      this.addmode = false;
      this.editmode = false;
      this.deletemode = true;
      this.listmode = false;
    } else {
      this.showWarning("Please Select a Record");
    }
    
}


editQuotesStatus(data){
  this.router.navigateByUrl('admin/status-management/quote/update/'+data.quote_status_id);
}


deleteQuotesStatus(){
  this.dataLoaded = false;
  this.adminService.deleteQuoteStatus(this.selectedData_ID).subscribe(data=>{
    if (data['success']) {
      this.showSuccess(data['msg']);
      this.cancel();
      this.selectedData_ID = null;
    } else {
      this.showError(data['msg']);
    }
  });
}



cancel(){
  $('#deleteModal').modal('hide');
  $('.addPartsRequestStatus').modal('hide');
  this.dataLoaded = false;
  this.addmode = true;
  this.editmode = false;
  this.deletemode = false;
  this.listmode = true;
  this.onPageReload();
}

onPageReload(){
  this.dtOptions = {
    pagingType: 'full_numbers',
    pageLength: 5,
    processing: true,
    ordering:true,
    lengthMenu : [5, 10, 25, 50, 100]
  };
  this.adminService.getQuoteStatusList().subscribe(async data=>{
    if (data['success']) {
      this.QuotesStatusList = await data['data'];
      this.dataLoaded = true;
    } else {
      this.QuotesStatusList = [];
    }
  });
}

showSuccess(msg) {
  Swal.fire({
    icon: 'success',
    title: 'Done',
    text: msg,
  })
}

showError(msg) {
  Swal.fire({
    icon: 'error',
    title: 'Error',
    text: msg,
  })
}

showWarning(msg) {
  Swal.fire({
    icon: 'warning',
    title: 'Check',
    text: msg,
  })
}
}
