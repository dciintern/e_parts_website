import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-addreason',
  templateUrl: './addreason.component.html',
  styleUrls: ['./addreason.component.scss']
})
export class AddreasonComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  ReasonList: any;
  dataLoaded: boolean;
  addmode: boolean;
  editmode: boolean;
  deletemode: boolean;
  listmode: boolean;
  selectedData_ID: any;
  addReasonForm: FormGroup;
  filterMode: Boolean = true;
  advancefilterMode: Boolean = false;
  p: number = 1;
  itemPage: number = 10;
  FilteredState: boolean = false;
  advanceFilterForm:FormGroup;
  constructor(private adminService:AdminModuleService, 
              private toastr:ToastrManager, private router:Router,
              private formBuilder:FormBuilder) {
                this.addReasonForm = this.formBuilder.group({
                  id:[''],
                  reason_name:['',Validators.required],
                  created_at:['',Validators.required],
                  created_by:['',Validators.required],
                  modified_at:['',Validators.required],
                  modified_by:['',Validators.required],
                });
               }

  ngOnInit(): void {

    this.dataLoaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      ordering:true,
      lengthMenu : [10, 25, 50, 100]
    };

    this.adminService.getReasonList().subscribe(async data=>{
      if (data['success']) {
        this.ReasonList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.ReasonList = [];
      }
    });
  }

  openAdvanceFilter(){
    $('.bd-example-modal-lg').modal('show')
  }

  addAdvanceFilter(){
    
  }

  ClearFilter(){
    this.FilteredState = false;
  }

  FilterData(){
    this.FilteredState = true;
    $('.bd-example-modal-lg').modal('hide')
  }

  pageChanged(event:any){
    this.p = event;
  }

  getFilterState(val:any){
    if (val == 'filterMode') {
      this.filterMode = true;
      this.advancefilterMode = false;
    } else if (val == 'advancefilterMode'){
      this.filterMode = false;
      this.advancefilterMode = true;
      this.openAdvanceFilter();
    }
  }

  getselected(event,i){
    console.log(i); 
  }

  getallselected(event){

  }


  addReason(){
    this.router.navigateByUrl('admin/reason-management/configure/add/0');
  }


  validate(id){
    if (id != null) {
      $('#deleteModal').modal('show');
      this.selectedData_ID =  id;
      this.addmode = false;
      this.editmode = false;
      this.deletemode = true;
      this.listmode = false;
    } else {
      this.showWarning("Please Select a Record");
    }
  }

  

  editReason(data){
    this.router.navigateByUrl('admin/reason-management/configure/update/'+data.id);
  }


  deleteReason(){
    this.dataLoaded = false;
    this.adminService.deleteReason(this.selectedData_ID).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showError(data['msg']);
      }
    });
  }

  updateReason(){
    this.dataLoaded = false;
    this.adminService.updateReason(this.addReasonForm.value).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showSuccess(data['msg']);
      }
    });
  }

  cancel(){
    $('#deleteModal').modal('hide');
    $('.addReason').modal('hide');
    this.dataLoaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;
    this.onPageReload();
  }

  onPageReload(){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };
    this.adminService.getReasonList().subscribe(async data=>{
      if (data['success']) {
        this.ReasonList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.ReasonList = [];
      }
    });
  }


  showSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: 'Done',
      text: msg,
    })
  }

  showError(msg) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: msg,
    })
  }

  showWarning(msg) {
    Swal.fire({
      icon: 'warning',
      title: 'Check',
      text: msg,
    })
  }
}
