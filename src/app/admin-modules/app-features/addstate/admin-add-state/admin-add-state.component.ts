import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-admin-add-state',
  templateUrl: './admin-add-state.component.html',
  styleUrls: ['./admin-add-state.component.scss']
})
export class AdminAddStateComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  StateList: any;
  dataLoaded: boolean;
  addmode: boolean;
  editmode: boolean;
  deletemode: boolean;
  listmode: boolean;
  selectedData_ID: any;
  addStateForm: FormGroup;
  id: string;
  type: string;
  countryList: any;
  constructor(private adminService:AdminModuleService, 
              private router:Router,
              private _Activatedroute:ActivatedRoute,
              private toastr:ToastrManager, 
              private formBuilder:FormBuilder) {
                this.addStateForm = this.formBuilder.group({
                  id:[''],
                  state_name:['',Validators.required],
                  status:['true',Validators.required],
                  country_id:['',Validators.required],
                  created_at:[''],
                  created_by:[''],
                  modified_at:[''],
                  modified_by:[''],
                });
               }

  ngOnInit(): void {

    this.dataLoaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;

    this._Activatedroute.paramMap.subscribe(params => { 
      console.log(params);
      this.id = params.get('id'); 
      this.type = params.get('type'); 
      if (this.type == 'update') {
        this.dataLoaded = true;
        this.addmode = false;
        this.editmode = true;
        this.deletemode = false;
        this.adminService.getStateInfo(this.id).subscribe(data=>{
          if (data['success']) {
            this.addStateForm.patchValue({
              id: data['data'].id,
              state_name: data['data'].state_name,
              country_id: data['data'].country_id,
              status: data['data'].status,
              created_at: data['data'].created_at,
              created_by: data['data'].created_by,
              modified_at: data['data'].modified_at,
              modified_by: data['data'].modified_by,
            });
            $('.addBanner').modal('show');
            this.selectedData_ID =  data['data'].id;
            this.addmode = false;
            this.editmode = true;
            this.deletemode = false;
            this.listmode = false; 
          } else {
            
          }
        })
      } else {
        this.dataLoaded = true;
        this.addmode = true;
        this.editmode = false;
        this.deletemode = false;
      }
    });

    this.adminService.getCountryList().subscribe(async data=>{
      if (data['success']) {
        this.countryList = await data['data'];
      } else {
        this.countryList = [];
      }
    });
  }

  getselected(event,i){
    console.log(i); 
  }

  getallselected(event){

  }


  addState(){
    if (this.addStateForm.valid) {
      this.adminService.createState(this.addStateForm.value).subscribe(data=>{
        if (data['success']) {
          this.showSuccess(data['msg']);
          this.cancel();
        } else {
          this.showError(data['msg']);
        }
      });
    } else {
      this.showWarning("Please Fill all Fields");
    }
  }


  validate(id){
    if (id != null) {
      $('#deleteModal').modal('show');
      this.selectedData_ID =  id;
      this.addmode = false;
      this.editmode = false;
      this.deletemode = true;
      this.listmode = false;
    } else {
      this.showWarning("Please Select a Record");
    }
  }

  

  editState(data){
    if (data != null) {
      this.addStateForm.patchValue({
        id: data.id,
        State_name: data.State_name,
        created_at: data.created_at,
        created_by: data.created_by,
        modified_at: data.modified_at,
        modified_by: data.modified_by,
      });
      $('.addState').modal('show');
      this.selectedData_ID =  data.id;
      this.addmode = false;
      this.editmode = true;
      this.deletemode = false;
      this.listmode = false;
    } else {
      this.showWarning("Please Select a Record");
    }
  }


  deleteState(){
    this.dataLoaded = false;
    this.adminService.deleteState(this.selectedData_ID).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showError(data['msg']);
      }
    });
  }

  updateState(){
    this.dataLoaded = false;
    this.adminService.updateState(this.addStateForm.value).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showSuccess(data['msg']);
      }
    });
  }

  cancel(){
    $('#deleteModal').modal('hide');
    $('.addState').modal('hide');
    this.dataLoaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;
    this.onPageReload();
    this.router.navigateByUrl('admin/state-management')
  }

  onPageReload(){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };
    this.adminService.getStateList().subscribe(async data=>{
      if (data['success']) {
        this.StateList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.StateList = [];
      }
    });
  }


  showSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: 'Done',
      text: msg,
    })
  }

  showError(msg) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: msg,
    })
  }

  showWarning(msg) {
    Swal.fire({
      icon: 'warning',
      title: 'Check',
      text: msg,
    })
  }
}
