import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-list-orders',
  templateUrl: './admin-list-orders.component.html',
  styleUrls: ['./admin-list-orders.component.scss']
})
export class AdminListOrdersComponent implements OnInit {
  dtOptions: DataTables.Settings = {};

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };
  }

  addNewOrder(){
    this.router.navigateByUrl('admin/order-management/add-order');
  }



  viewOrder(id){
    this.router.navigateByUrl('admin/order-management/view-order');
  }
}
