import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-admin-sms-template-management',
  templateUrl: './admin-sms-template-management.component.html',
  styleUrls: ['./admin-sms-template-management.component.scss']
})
export class AdminSmsTemplateManagementComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  SMSList: any;
  dataLoaded: boolean;
  deletemode: boolean;
  listmode: boolean;
  selectedData_ID: any;
  filterMode: Boolean = true;
  advancefilterMode: Boolean = false;
  p: number = 1;
  itemPage: number = 10;
  FilteredState:Boolean = false;
  constructor(private adminService:AdminModuleService, private toastr:ToastrManager, private router:Router) {}

  ngOnInit(): void {

    this.dataLoaded = false;
    this.deletemode = false;
    this.listmode = true;
    this.filterMode = true;
    this.advancefilterMode = false;
    this.FilteredState = false;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };

    this.adminService.getSMSList().subscribe(async data=>{
      if (data['success']) {
        this.SMSList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.SMSList = [];
      }
    });
  }

  ClearFilter(){
    this.FilteredState = false;
  }

  FilterData(){
    this.FilteredState = true;
  }

  pageChanged(event:any){
    this.itemPage = event.target.value;
  }

  getFilterState(val:any){
    if (val == 'filterMode') {
      this.filterMode = true;
      this.advancefilterMode = false;
    } else if (val == 'advancefilterMode'){
      this.filterMode = false;
      this.advancefilterMode = true;
    }
  }

  getselected(event,i){
    console.log(i); 
  }

  getallselected(event){

  }


  addSMS(){
    this.router.navigateByUrl('admin/templates-management/sms-templates-management/configure/add/0');
  }


  validate(id){
    if (id != null) {
      $('#deleteModal').modal('show');
      this.selectedData_ID =  id;
      this.deletemode = true;
      this.listmode = false;
    } else {
      this.showWarning("Please Select a Record");
    }
  }

  

  editSMS(data){
    this.router.navigateByUrl('admin/templates-management/sms-templates-management/configure/update/'+data.id);
  }


  deleteSMS(){
    this.dataLoaded = false;
    this.adminService.deleteSMS(this.selectedData_ID).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showError(data['msg']);
      }
    });
  }

  cancel(){
    $('#deleteModal').modal('hide');
    $('.addSMS').modal('hide');
    this.dataLoaded = false;
    this.deletemode = false;
    this.listmode = true;
    this.onPageReload();
  }

  onPageReload(){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };
    this.adminService.getSMSList().subscribe(async data=>{
      if (data['success']) {
        this.SMSList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.SMSList = [];
      }
    });
  }


  showSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: 'Done',
      text: msg,
    })
  }

  showError(msg) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: msg,
    })
  }

  showWarning(msg) {
    Swal.fire({
      icon: 'warning',
      title: 'Check',
      text: msg,
    })
  }
}
