 import { Component, OnInit } from '@angular/core';
  import { FormGroup, FormBuilder, Validators } from '@angular/forms';
  import { Router } from '@angular/router';
  import { ToastrManager } from 'ng6-toastr-notifications';
  import { AdminModuleService } from 'src/app/admin-module.service';
  import { environment } from 'src/environments/environment.prod';
  declare var $:any;
  
  
@Component({
  selector: 'app-admin-place-of-supply',
  templateUrl: './admin-place-of-supply.component.html',
  styleUrls: ['./admin-place-of-supply.component.scss']
})
export class AdminPlaceOfSupplyComponent implements OnInit {
  
    dtOptions: DataTables.Settings = {};
    CategoryRequestStatusList: any;
    dataLoaded: boolean;
    addmode: boolean;
    editmode: boolean;
    listmode: boolean;
    selectedData_ID: any;
    addCategoryRequestStatusForm: FormGroup;
    deletemode: boolean;
    imageUploaded: boolean;
  
    constructor(private adminService:AdminModuleService, private router:Router,
                private toastr:ToastrManager, 
                private formBuilder:FormBuilder) {
                  this.addCategoryRequestStatusForm = this.formBuilder.group({
                    pos_id: ['', Validators.required],
                    tax_name: ['', Validators.required],
                    tax_code: ['', Validators.required],
                    erp_id: ['', Validators.required],
                    last_integrated_date: ['', Validators.required],
                    status: ['', Validators.required],
                    created_at: ['', Validators.required],
                    created_by: ['', Validators.required],
                    modified_at: ['', Validators.required],
                    modified_by: ['', Validators.required],
                  });
                 }
  
    ngOnInit(): void {
  
      this.dataLoaded = false;
      this.imageUploaded = false;
      this.addmode = true;
      this.editmode = false;
      this.deletemode = false;
      this.listmode = true;
  
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 5,
        processing: true,
        ordering:true,
        lengthMenu : [5, 10, 25, 50, 100]
      };
  
  
      this.adminService.getCategoryRequestStatusList().subscribe(async data=>{
        if (data['success']) {
          this.CategoryRequestStatusList = await data['data'];
          this.dataLoaded = true;
        } else {
          this.CategoryRequestStatusList = [];
        }
      });
    }
  
    getselected(event,i){
      console.log(i); 
    }
  
    getallselected(event){
  
    }
  
    addplaceofsupply(){
      this.router.navigateByUrl('admin/tax-management/place-of-supply/add/0');
    }
  
  
    validate(id){
      if (id != null) {
        $('#deleteModal').modal('show');
        this.selectedData_ID =  id;
        this.addmode = false;
        this.editmode = false;
        this.deletemode = true;
        this.listmode = false;
      } else {
        this.showWarning("Please Select a Record");
      }
    }
  
    
  
    editCategoryRequestStatus(data){
      this.router.navigateByUrl('admin/status-management/category-request/update/'+data.id);
    }
  
  
    deleteCategoryRequestStatus(){
      this.dataLoaded = false;
      this.adminService.deleteCategoryRequestStatus(this.selectedData_ID).subscribe(data=>{
        if (data['success']) {
          this.showSuccess(data['msg']);
          this.cancel();
          this.selectedData_ID = null;
        } else {
          this.showError(data['msg']);
        }
      });
    }
  
    updateCategoryRequestStatus(){
      this.dataLoaded = false;
      this.adminService.updateCategoryRequestStatus(this.addCategoryRequestStatusForm.value).subscribe(data=>{
        if (data['success']) {
          this.showSuccess(data['msg']);
          this.cancel();
          this.selectedData_ID = null;
        } else {
          this.showSuccess(data['msg']);
        }
      });
    }
  
    cancel(){
      $('#deleteModal').modal('hide');
      $('.addCategoryRequestStatus').modal('hide');
      this.dataLoaded = false;
      this.addmode = true;
      this.editmode = false;
      this.deletemode = false;
      this.listmode = true;
      this.imageUploaded = true;
      this.onPageReload();
    }
  
    onPageReload(){
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 5,
        processing: true,
        ordering:true,
        lengthMenu : [5, 10, 25, 50, 100]
      };
      this.adminService.getCategoryRequestStatusList().subscribe(async data=>{
        if (data['success']) {
          this.CategoryRequestStatusList = await data['data'];
          this.dataLoaded = true;
        } else {
          this.CategoryRequestStatusList = [];
        }
      });
    }
  
  
    showSuccess(msg) {
      this.toastr.successToastr(msg);
    }
  
    showError(msg) {
      this.toastr.errorToastr(msg);
    }
  
    showWarning(msg) {
      this.toastr.warningToastr(msg);
    }
  }
  
