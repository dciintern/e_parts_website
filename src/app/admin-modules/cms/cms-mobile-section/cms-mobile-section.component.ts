import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-cms-mobile-section',
  templateUrl: './cms-mobile-section.component.html',
  styleUrls: ['./cms-mobile-section.component.scss']
})
export class CmsMobileSectionComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  MobileSectionList: any;
  dataLoaded: boolean;
  addmode: boolean;
  editmode: boolean;
  listmode: boolean;
  selectedData_ID: any;
  addMobileSectionForm: FormGroup;
  deletemode: boolean;
  imageUploaded: boolean;
  MobileSectionUrl: string;
  FilteredState: boolean;
  filterMode: boolean;
  advancefilterMode: boolean;
  p: number = 1;
  itemPage: number = 10;
  advanceFilterForm: FormGroup;

  constructor(private adminService:AdminModuleService, private router:Router, 
              private toastr:ToastrManager, private formBuilder:FormBuilder) {
                this.addMobileSectionForm = this.formBuilder.group({
                  id:[''],
                  title:['',Validators.required],
                  content:['',Validators.required],
                  icon:['',Validators.required],
                  status:['',Validators.required],
                  created_at:['',Validators.required],
                  created_by:['',Validators.required],
                  modified_at:['',Validators.required],
                  modified_by:['',Validators.required],
                });
               }

  ngOnInit(): void {

    this.dataLoaded = false;
    this.imageUploaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };


    this.adminService.getMobileSectionList().subscribe(async data=>{
      if (data['success']) {
        this.MobileSectionList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.MobileSectionList = [];
      }
    });
  }

  openAdvanceFilter(){
    $('.bd-example-modal-lg').modal('show')
  }

  addAdvanceFilter(){
    
  }

  ClearFilter(){
    this.FilteredState = false;
  }

  FilterData(){
    this.FilteredState = true;
    $('.bd-example-modal-lg').modal('hide')
  }

  pageChanged(event:any){
    this.itemPage = event.target.value;
  }

  getFilterState(val:any){
    if (val == 'filterMode') {
      this.filterMode = true;
      this.advancefilterMode = false;
    } else if (val == 'advancefilterMode'){
      this.filterMode = false;
      this.advancefilterMode = true;
      this.openAdvanceFilter();
    }
  }

  getselected(event,i){
    console.log(i); 
  }

  getallselected(event){

  }


  addMobileSection(){
    this.router.navigateByUrl('admin/cms/mobile-section/add/0');
  }



  editMobileSection(data){
    this.router.navigateByUrl('admin/cms/mobile-section/update/'+data.id);
  }

  validate(id){
    if (id != null) {
      $('#deleteModal').modal('show');
      this.selectedData_ID =  id;
      this.addmode = false;
      this.editmode = false;
      this.deletemode = true;
      this.listmode = false;
    } else {
      this.showWarning("Please Select a Record");
    }
  }


  deleteMobileSection(){
    this.dataLoaded = false;
    this.adminService.deleteMobileSection(this.selectedData_ID).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showError(data['msg']);
      }
    });
  }



  cancel(){
    $('#deleteModal').modal('hide');
    $('.addFeatures').modal('hide');
    this.dataLoaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;
    this.onPageReload();
  }

  onPageReload(){
    this.adminService.getMobileSectionList().subscribe(async data=>{
      if (data['success']) {
        this.MobileSectionList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.MobileSectionList = [];
      }
    });
  }




  showSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: 'Done',
      text: msg,
    })
  }

  showError(msg) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: msg,
    })
  }

  showWarning(msg) {
    Swal.fire({
      icon: 'warning',
      title: 'Check',
      text: msg,
    })
  }
}
