import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import { environment } from 'src/environments/environment.prod';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-cms-features',
  templateUrl: './cms-features.component.html',
  styleUrls: ['./cms-features.component.scss']
})
export class CmsFeaturesComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  FeaturesList: any;
  dataLoaded: boolean;
  addmode: boolean;
  editmode: boolean;
  listmode: boolean;
  selectedData_ID: any;
  addFeaturesForm: FormGroup;
  deletemode: boolean;
  imageUploaded: boolean;
  featuresUrl: string;
  filterMode: Boolean = true;
  advancefilterMode: Boolean = false;
  p: number = 1;
  itemPage: number = 10;
  FilteredState: boolean = false;
  advanceFilterForm:FormGroup;

  constructor(private adminService:AdminModuleService, private router:Router,
              private toastr:ToastrManager, 
              private formBuilder:FormBuilder) {
                this.addFeaturesForm = this.formBuilder.group({
                  id:[''],
                  title:['',Validators.required],
                  content:['',Validators.required],
                  icon:['',Validators.required],
                  status:['',Validators.required],
                  created_at:['',Validators.required],
                  created_by:['',Validators.required],
                  modified_at:['',Validators.required],
                  modified_by:['',Validators.required],
                });
               }

  ngOnInit(): void {

    this.dataLoaded = false;
    this.imageUploaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;



      // var $th = $('.tableFixHead').find('thead th')
      // $('.tableFixHead').on('scroll', function() {
      //   $th.css('transform', 'translateY('+ this.scrollTop +'px)');
      // });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };


    this.adminService.getFeaturesList().subscribe(async data=>{
      if (data['success']) {
        this.FeaturesList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.FeaturesList = [];
      }
    });
  }

  openAdvanceFilter(){
    $('.bd-example-modal-lg').modal('show')
  }

  addAdvanceFilter(){
    
  }

  ClearFilter(){
    this.FilteredState = false;
  }

  FilterData(){
    this.FilteredState = true;
    $('.bd-example-modal-lg').modal('hide')
  }


  getFilterState(val:any){
    if (val == 'filterMode') {
      this.filterMode = true;
      this.advancefilterMode = false;
    } else if (val == 'advancefilterMode'){
      this.filterMode = false;
      this.advancefilterMode = true;
      this.openAdvanceFilter();
    }
  }

  pageChanged(event:any){
    this.p = event;
  }

  getselected(event,i){
    console.log(i); 
  }

  getallselected(event){

  }

  UploadFeatures(event){
    this.adminService.uploadFeaturesCms(event.target.files[0]).subscribe(async data=>{
      if (data['success']) {
        this.showSuccess(data['message']);
        this.featuresUrl = await environment.baseurl+"cms-main/features_cms/viewimage?filename="+data['uploadedfile'];
        this.addFeaturesForm.patchValue({
          icon:environment.baseurl+"cms-main/features_cms/viewimage?filename="+data['uploadedfile']
        });
        this.imageUploaded = true;
      } else {
        this.showError(data['message']);
        this.imageUploaded = false;
      }
    });
  }


  addFeatures(){
    this.router.navigateByUrl('admin/cms/features/add/0');
  }


  validate(id){
    if (id != null) {
      $('#deleteModal').modal('show');
      this.selectedData_ID =  id;
      this.addmode = false;
      this.editmode = false;
      this.deletemode = true;
      this.listmode = false;
    } else {
      this.showWarning("Please Select a Record");
    }
  }

  

  editFeatures(data){
    this.router.navigateByUrl('admin/cms/features/update/'+data.id);
  }

  cloneFeatures(data){
    this.router.navigateByUrl('admin/cms/features/clone/'+data.id);
  }


  deleteFeatures(){
    this.dataLoaded = false;
    this.adminService.deleteFeatures(this.selectedData_ID).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showError(data['msg']);
      }
    });
  }

  updateFeatures(){
    this.dataLoaded = false;
    this.adminService.updateFeatures(this.addFeaturesForm.value).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['msg']);
        this.cancel();
        this.selectedData_ID = null;
      } else {
        this.showSuccess(data['msg']);
      }
    });
  }

  cancel(){
    $('#deleteModal').modal('hide');
    $('.addFeatures').modal('hide');
    this.dataLoaded = false;
    this.addmode = true;
    this.editmode = false;
    this.deletemode = false;
    this.listmode = true;
    this.imageUploaded = true;
    this.featuresUrl = null;
    this.onPageReload();
  }

  onPageReload(){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      ordering:true,
      lengthMenu : [5, 10, 25, 50, 100]
    };
    this.adminService.getFeaturesList().subscribe(async data=>{
      if (data['success']) {
        this.FeaturesList = await data['data'];
        this.dataLoaded = true;
      } else {
        this.FeaturesList = [];
      }
    });
  }


  showSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: 'Done',
      text: msg,
    })
  }

  showError(msg) {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: msg,
    })
  }

  showWarning(msg) {
    Swal.fire({
      icon: 'warning',
      title: 'Check',
      text: msg,
    })
  }
}
