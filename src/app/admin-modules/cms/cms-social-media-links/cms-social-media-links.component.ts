import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import Swal from 'sweetalert2';
declare var $:any;

@Component({
  selector: 'app-cms-social-media-links',
  templateUrl: './cms-social-media-links.component.html',
  styleUrls: ['./cms-social-media-links.component.scss']
})
export class CmsSocialMediaLinksComponent implements OnInit {

    dtOptions: DataTables.Settings = {};
    medialinksList: any;
    dataLoaded: boolean;
    addmode: boolean;
    editmode: boolean;
    listmode: boolean;
    selectedData_ID: any;
    addmedialinksForm: FormGroup;
    deletemode: boolean;
    imageUploaded: boolean;
    medialinksUrl: string;
    filterMode: Boolean = true;
    advancefilterMode: Boolean = false;
    p: number = 1;
    itemPage: number = 10;
    FilteredState: boolean = false;
    advanceFilterForm:FormGroup;
  
    constructor(private adminService:AdminModuleService, private router:Router, 
                private toastr:ToastrManager, private formBuilder:FormBuilder) {
                  this.addmedialinksForm = this.formBuilder.group({
                    id:[''],
                    title:['',Validators.required],
                    content:['',Validators.required],
                    icon:['',Validators.required],
                    status:['',Validators.required],
                    created_at:['',Validators.required],
                    created_by:['',Validators.required],
                    modified_at:['',Validators.required],
                    modified_by:['',Validators.required],
                  });
                 }
  
    ngOnInit(): void {
  
      this.dataLoaded = false;
      this.imageUploaded = false;
      this.addmode = true;
      this.editmode = false;
      this.deletemode = false;
      this.listmode = true;
  
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 5,
        processing: true,
        ordering:true,
        lengthMenu : [5, 10, 25, 50, 100]
      };
  
  
      this.adminService.getMediaLinksList().subscribe(async data=>{
        if (data['success']) {
          this.medialinksList = await data['data'];
          this.dataLoaded = true;
        } else {
          this.medialinksList = [];
        }
      });
    }

    openAdvanceFilter(){
      $('.bd-example-modal-lg').modal('show')
    }
  
    addAdvanceFilter(){
      
    }
  
    ClearFilter(){
      this.FilteredState = false;
    }
  
    FilterData(){
      this.FilteredState = true;
      $('.bd-example-modal-lg').modal('hide')
    }
  
  
    getFilterState(val:any){
      if (val == 'filterMode') {
        this.filterMode = true;
        this.advancefilterMode = false;
      } else if (val == 'advancefilterMode'){
        this.filterMode = false;
        this.advancefilterMode = true;
        this.openAdvanceFilter();
      }
    }
  
    pageChanged(event:any){
      this.p = event;
    }
  
    getselected(event,i){
      console.log(i); 
    }
  
    getallselected(event){
  
    }
  

    addMediaLinks(){
      this.router.navigateByUrl('admin/cms/social-media-links/add/0');
    }
  
  
  
    editMediaLinks(data){
      this.router.navigateByUrl('admin/cms/social-media-links/update/'+data.id);
    }

    validate(id){
      if (id != null) {
        $('#deleteModal').modal('show');
        this.selectedData_ID =  id;
        this.addmode = false;
        this.editmode = false;
        this.deletemode = true;
        this.listmode = false;
      } else {
        this.showWarning("Please Select a Record");
      }
    }


    deleteMediaLinks(){
      this.dataLoaded = false;
      this.adminService.deleteMediaLinks(this.selectedData_ID).subscribe(data=>{
        if (data['success']) {
          this.showSuccess(data['msg']);
          this.cancel();
          this.selectedData_ID = null;
        } else {
          this.showError(data['msg']);
        }
      });
    }

    onPageReload(){
      this.adminService.getMediaLinksList().subscribe(async data=>{
        if (data['success']) {
          this.medialinksList = await data['data'];
          this.dataLoaded = true;
        } else {
          this.medialinksList = [];
        }
      });
    }
  

  
    cancel(){
      $('#deleteModal').modal('hide');
      $('.addFeatures').modal('hide');
      this.dataLoaded = false;
      this.addmode = true;
      this.editmode = false;
      this.deletemode = false;
      this.listmode = true;
      this.onPageReload();
    }
  

  
  
    showSuccess(msg) {
      Swal.fire({
        icon: 'success',
        title: 'Done',
        text: msg,
      })
    }
  
    showError(msg) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: msg,
      })
    }
  
    showWarning(msg) {
      Swal.fire({
        icon: 'warning',
        title: 'Check',
        text: msg,
      })
    }
  
}
