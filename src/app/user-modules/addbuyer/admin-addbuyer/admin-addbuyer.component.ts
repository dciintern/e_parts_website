import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AdminModuleService } from 'src/app/admin-module.service';
import { BuyersService } from 'src/app/buyers.service';
import { environment } from 'src/environments/environment.prod';

declare var $:any;

@Component({
  selector: 'app-admin-addbuyer',
  templateUrl: './admin-addbuyer.component.html',
  styleUrls: ['./admin-addbuyer.component.scss']
})
export class AdminAddbuyerComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  addmode: boolean;
  editmode: boolean;
  listmode: boolean;
  BuyersForm: FormGroup;
  Hubs: any;
  Districts: any;
  hubsFilter: any = { hubname: '' };
  Buyers: any;
  countryList: any;
  fileUploaded:boolean=true;
  form1:boolean= false;
  form2:boolean= false;
  logoUrl: string;

  constructor(private formBuilder:FormBuilder, private BuyersService:BuyersService, private adminService:AdminModuleService,
              private toastr:ToastrManager) { 
    this.BuyersForm = this.formBuilder.group({
      id:['', Validators.required],
      user_id:[undefined, Validators.required],
      name:[undefined, Validators.required],
      company_logo:[undefined, Validators.required],
      subscription_plan_id:[undefined, Validators.required],
      status:[undefined, Validators.required],
      b_country:[undefined, Validators.required],
      b_state:[undefined, Validators.required],
      b_city:[undefined, Validators.required],
      b_address1:[undefined, Validators.required],
      b_address2:[undefined, Validators.required],
      b_displayname:[undefined, Validators.required],
      b_phonenumber:[undefined, Validators.required],
      b_countrycode:[undefined, Validators.required],
      sh_country:[undefined, Validators.required],
      sh_state:[undefined, Validators.required],
      sh_city:[undefined, Validators.required],
      sh_address1:[undefined, Validators.required],
      sh_address2:[undefined, Validators.required],
      sh_display_name:[undefined, Validators.required],
      sh_phone_number:[undefined, Validators.required],
      sh_country_code:[undefined, Validators.required],
      sh_phone_verified:[undefined, Validators.required],
      p_address:[undefined, Validators.required],
      pick_address_map:[undefined, Validators.required],
      payment_mode:[undefined, Validators.required],
      product_type:[undefined, Validators.required],
      trading_type:[undefined, Validators.required],
      business_location:[undefined, Validators.required],
      authenticated:[undefined, Validators.required],
      residenceid:[undefined, Validators.required],
      expiry_date:[undefined, Validators.required],
      country_of_issue:[undefined, Validators.required],
      first_name:[undefined, Validators.required],
      middle_name:[undefined, Validators.required],
      last_name:[undefined, Validators.required],
      dob:[undefined, Validators.required],
      reg_name:[undefined, Validators.required],
      license_number:[undefined, Validators.required],
      address_line:[undefined, Validators.required],
      doc_front:[undefined, Validators.required],
      doc_back:[undefined, Validators.required],
      doc_license:[undefined, Validators.required],
      doc_regstatus:[undefined, Validators.required],
      doc_failreason:[undefined, Validators.required],
      taxregnumber:[undefined, Validators.required],
      doc_taxcertificate:[undefined, Validators.required],
      beneficiary_name:[undefined, Validators.required],
      bankname:[undefined, Validators.required],
      branchname:[undefined, Validators.required],
      baccountnumber:[undefined, Validators.required],
      bibannumber:[undefined, Validators.required],
      bswiftcode:[undefined, Validators.required],
      bcurrency:[undefined, Validators.required],
      buploadedfile:[undefined, Validators.required],
      wh_country:[undefined, Validators.required],
      wh_state:[undefined, Validators.required],
      wh_city:[undefined, Validators.required],
      wh_address1:[undefined, Validators.required],
      wh_address2:[undefined, Validators.required],
      wh_displaynumber:[undefined, Validators.required],
      wh_phonenumber:[undefined, Validators.required],
      wh_countrycode:[undefined, Validators.required],
      wh_phoneverified:[undefined, Validators.required],
      wh_addressline:[undefined, Validators.required],
      wh_pickaddress:[undefined, Validators.required],
    });
  }

  ngOnInit(): void {
    
    this.addmode = true;
    this.editmode = false;
    this.BuyersService.listBuyers().subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['message']);
        this.Buyers = data['data'];
      } else {
        this.showError(data['message']);
        this.Buyers = null;
      }
    });
    this.adminService.getCountryList().subscribe(async data=>{
      if (data['success']) {
        this.countryList = await data['data'];
      } else {
        this.countryList = [];
      }
    });
  }


  addBuyers(){
    this.BuyersForm.patchValue({
      created:new Date(),
      status:'active'
    });
    this.BuyersService.addBuyers(this.BuyersForm.value).subscribe(data=>{
      if (data['success']) {
        this.recall();
        this.showSuccess(data['message']);
      } else {
        this.showError(data['message']);
      }
    });
  }

  editBuyers(){
    const selectedNodes = null;
    const selectedData = selectedNodes.map(node => node.data );
    if (selectedData.length == 1) {
      this.addmode = false;
      this.editmode = true;
      $('#addBuyers').modal('show');
      this.BuyersForm.patchValue({
        id:selectedData[0].id,
        user_id:selectedData[0].user_id,
        name:selectedData[0].name,
        company_logo:selectedData[0].company_logo,
        subscription_plan_id:selectedData[0].subscription_plan_id,
        status:selectedData[0].status,
        b_country:selectedData[0].b_country,
        b_state:selectedData[0].b_state,
        b_city:selectedData[0].b_city,
        b_address1:selectedData[0].b_address1,
        b_address2:selectedData[0].b_address2,
        b_displayname:selectedData[0].b_displayname,
        b_phonenumber:selectedData[0].b_phonenumber,
        b_countrycode:selectedData[0].b_countrycode,
        sh_country:selectedData[0].sh_country,
        sh_state:selectedData[0].sh_state,
        sh_city:selectedData[0].sh_city,
        sh_address1:selectedData[0].sh_address1,
        sh_address2:selectedData[0].sh_address2,
        sh_display_name:selectedData[0].sh_display_name,
        sh_phone_number:selectedData[0].sh_phone_number,
        sh_country_code:selectedData[0].sh_country_code,
        sh_phone_verified:selectedData[0].sh_phone_verified,
        p_address:selectedData[0].p_address,
        pick_address_map:selectedData[0].pick_address_map,
        payment_mode:selectedData[0].payment_mode,
        product_type:selectedData[0].product_type,
        trading_type:selectedData[0].trading_type,
        business_location:selectedData[0].business_location,
        authenticated:selectedData[0].authenticated,
        residenceid:selectedData[0].residenceid,
        expiry_date:selectedData[0].expiry_date,
        country_of_issue:selectedData[0].country_of_issue,
        first_name:selectedData[0].first_name,
        middle_name:selectedData[0].middle_name,
        last_name:selectedData[0].last_name,
        dob:selectedData[0].dob,
        reg_name:selectedData[0].reg_name,
        license_number:selectedData[0].license_number,
        address_line:selectedData[0].address_line,
        doc_front:selectedData[0].doc_front,
        doc_back:selectedData[0].doc_back,
        doc_license:selectedData[0].doc_license,
        doc_regstatus:selectedData[0].doc_regstatus,
        doc_failreason:selectedData[0].doc_failreason,
        taxregnumber:selectedData[0].taxregnumber,
        doc_taxcertificate:selectedData[0].doc_taxcertificate,
        beneficiary_name:selectedData[0].beneficiary_name,
        bankname:selectedData[0].bankname,
        branchname:selectedData[0].branchname,
        baccountnumber:selectedData[0].baccountnumber,
        bibannumber:selectedData[0].bibannumber,
        bswiftcode:selectedData[0].bswiftcode,
        bcurrency:selectedData[0].bcurrency,
        buploadedfile:selectedData[0].buploadedfile
      });
    } else {
      this.showError("Select only One record");
    }
    
  }

  updateBuyers(){
    this.BuyersService.updateBuyers(this.BuyersForm.value).subscribe(data=>{
      if (data['success']) {
        this.recall();
        this.showSuccess(data['message']);
      } else {
        this.showError(data['message']);
      }
    });
  }

  Validate(){
    const selectedNodes = null;
    const selectedData = selectedNodes.map(node => node.data );

    if (selectedData.length > 0) {
      $('#deleteBuyers').modal('show');
    } else {
      this.showError("Select atleast One record");
    }

  }

  deleteBuyers(){
    const selectedNodes = null;
    const selectedData = selectedNodes.map(node => node.data );

    for (let i = 0; i < selectedData.length; i++) {
      this.BuyersService.deleteBuyers(selectedData[i].uid).subscribe(data=>{
        if (data['success']) {
          this.showSuccess(data['message']);
        } else {
          this.showError(data['message']);
        }
      });
    }
    this.recall();
  }

  cancel(){
    this.BuyersForm.reset();
    this.editmode = false;
    this.addmode = true;
  }

  recall(){
    this.BuyersForm.reset();
    this.BuyersService.listBuyers().subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['message']);
        this.Buyers = data['data'];
      } else {
        this.showError(data['message']);
        this.Buyers = null;
      }
    });
  }

  showSuccess(msg) {
    this.toastr.successToastr(msg);
  }

  showError(msg) {
    this.toastr.errorToastr(msg);
  }

  showWarning(msg) {
    this.toastr.warningToastr(msg);
  }


  UploadFile(event){
    this.fileUploaded = false;
    this.adminService.uploadLogoCms(event.target.files[0]).subscribe(data=>{
      if (data['success']) {
        this.showSuccess(data['message']);
        this.logoUrl = environment.baseurl+"cms-main/viewimage?filename="+data['uploadedfile'];
        this.BuyersForm.patchValue({
          company_logo:environment.baseurl+"cms-main/viewimage?filename="+data['uploadedfile']
        });
        this.fileUploaded = true;
        this.formnext()
      } else {
        this.showError(data['message']);
        this.fileUploaded = false;
      }
    });
  }
  formnext(){
    console.log(this.logoUrl)
    console.log(this.BuyersForm.value.subscription_plan_id)
    console.log(this.BuyersForm.value.status)

    if(this.BuyersForm.value.name != undefined && this.BuyersForm.value.subscription_plan_id != undefined && this.BuyersForm.value.status != undefined && this.logoUrl != undefined){
      this.form1 = true
      console.log(this.BuyersForm.value.name)
      console.log(this.BuyersForm.value.subscription_plan_id)
      console.log(this.BuyersForm.value.status)
    }
    else{
      this.form1 = false
    }
  }
  next(){
    this.form2 = true
    console.log(this.form2)
  }
}
