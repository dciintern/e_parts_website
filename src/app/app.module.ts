import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common'; 
import { ToastrModule } from 'ng6-toastr-notifications';
import { AgGridModule } from 'ag-grid-angular';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularEditorModule } from '@kolkov/angular-editor';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterPipeModule } from 'ngx-filter-pipe';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { DataTablesModule } from 'angular-datatables';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsModule } from 'ng2-charts';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { EcellsComponent } from './ecells/ecells.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddbuyersComponent } from './addbuyers/addbuyers.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddsellersComponent } from './addsellers/addsellers.component';
import { AddsettingsComponent } from './addsettings/addsettings.component';
import { AddintermedaitorComponent } from './addintermedaitor/addintermedaitor.component';
import { AddcarsComponent } from './addcars/addcars.component';
import { LandingComponent } from './landing/landing.component';
import { NavigationComponent } from './user-pages/navigation/navigation.component';
import { HomeComponent } from './user-pages/home/home.component';
import { FooterComponent } from './user-pages/footer/footer.component';
import { ContactComponent } from './user-pages/contact/contact.component';
import { PricingComponent } from './user-pages/pricing/pricing.component';
import { ForgotPasswordComponent } from './user-pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './user-pages/reset-password/reset-password.component';
import { AddbuyerComponent } from './user-modules/addbuyer/addbuyer.component';
import { AddsellerComponent } from './user-modules/addseller/addseller.component';
import { AddintermediatorComponent } from './user-modules/addintermediator/addintermediator.component';
import { AddcityComponent } from './admin-modules/app-features/addcity/addcity.component';
import { AddcountryComponent } from './admin-modules/app-features/addcountry/addcountry.component';
import { AddcurrencyComponent } from './admin-modules/app-features/addcurrency/addcurrency.component';
import { AddpayoutScheduleComponent } from './admin-modules/app-features/addpayout-schedule/addpayout-schedule.component';
import { AddproductTypeComponent } from './admin-modules/app-features/addproduct-type/addproduct-type.component';
import { AddreasonComponent } from './admin-modules/app-features/addreason/addreason.component';
import { AddroleComponent } from './admin-modules/app-features/addrole/addrole.component';
import { AddstateComponent } from './admin-modules/app-features/addstate/addstate.component';
import { AddsubscriptionPlanComponent } from './admin-modules/app-features/addsubscription-plan/addsubscription-plan.component';
import { AddtradingTypeComponent } from './admin-modules/app-features/addtrading-type/addtrading-type.component';
import { AdduserTypeComponent } from './admin-modules/app-features/adduser-type/adduser-type.component';
import { CmsInfoComponent } from './admin-modules/cms/cms-info/cms-info.component';
import { CmsBannerComponent } from './admin-modules/cms/cms-banner/cms-banner.component';
import { CmsFeaturesComponent } from './admin-modules/cms/cms-features/cms-features.component';
import { CmsHeroSectionComponent } from './admin-modules/cms/cms-hero-section/cms-hero-section.component';
import { CmsServicesComponent } from './admin-modules/cms/cms-services/cms-services.component';
import { CmsFooterComponent } from './admin-modules/cms/cms-footer/cms-footer.component';
import { AddUsersComponent } from './admin-modules/app-users/add-users/add-users.component';
import { CmsBuyerSellerComponent } from './admin-modules/cms/cms-buyer-seller/cms-buyer-seller.component';
import { BuyerMainLayoutComponent } from './buyer-modules/layouts/buyer-main-layout/buyer-main-layout.component';
import { SellerMainLayoutComponent } from './seller-modules/layouts/seller-main-layout/seller-main-layout.component';
import { BuyerDashboardComponent } from './buyer-modules/layouts/buyer-dashboard/buyer-dashboard.component';
import { SellerDashboardComponent } from './seller-modules/layouts/seller-dashboard/seller-dashboard.component';
import { ViewOrdersComponent } from './seller-modules/order-management/view-orders/view-orders.component';
import { ListOrdersComponent } from './seller-modules/order-management/list-orders/list-orders.component';
import { ListProductsComponent } from './seller-modules/product-management/list-products/list-products.component';
import { ViewProductComponent } from './seller-modules/product-management/view-product/view-product.component';
import { ListTeamsComponent } from './seller-modules/team-management/list-teams/list-teams.component';
import { ViewTeamComponent } from './seller-modules/team-management/view-team/view-team.component';
import { OrderManagementComponent } from './seller-modules/order-management/order-management.component';
import { ProductManagementComponent } from './seller-modules/product-management/product-management.component';
import { TeamManagementComponent } from './seller-modules/team-management/team-management.component';
import { BuyerHomeComponent } from './buyer-modules/layouts/buyer-pages/buyer-home/buyer-home.component';
import { BuyerPagesComponent } from './buyer-modules/layouts/buyer-pages/buyer-pages.component';
import { BuyerNavigationComponent } from './buyer-modules/layouts/buyer-pages/buyer-navigation/buyer-navigation.component';
import { BuyerOrderManagementComponent } from './buyer-modules/buyer-order-management/buyer-order-management.component';
import { BuyerListOrdersComponent } from './buyer-modules/buyer-order-management/buyer-list-orders/buyer-list-orders.component';
import { BuyerViewOrderComponent } from './buyer-modules/buyer-order-management/buyer-view-order/buyer-view-order.component';
import { AdminOrderManagementComponent } from './admin-modules/admin-order-management/admin-order-management.component';
import { AdminViewOrderDetailsComponent } from './admin-modules/admin-order-management/admin-view-order-details/admin-view-order-details.component';
import { AdminListOrdersComponent } from './admin-modules/admin-order-management/admin-list-orders/admin-list-orders.component';
import { AdminAddBannerComponent } from './admin-modules/cms/cms-banner/admin-add-banner/admin-add-banner.component';
import { AdminAddFeaturesComponent } from './admin-modules/cms/cms-features/admin-add-features/admin-add-features.component';
import { AdminAddServicesComponent } from './admin-modules/cms/cms-services/admin-add-services/admin-add-services.component';
import { AdminAddsellerComponent } from './user-modules/addseller/admin-addseller/admin-addseller.component';
import { AdminAddbuyerComponent } from './user-modules/addbuyer/admin-addbuyer/admin-addbuyer.component';
import { AdminAddintermediatorComponent } from './user-modules/addintermediator/admin-addintermediator/admin-addintermediator.component';
import { AdminBrandManagementComponent } from './admin-modules/admin-parts-management/admin-brand-management/admin-brand-management.component';
import { AdminCarManagementComponent } from './admin-modules/admin-parts-management/admin-car-management/admin-car-management.component';
import { AdminGroupManagementComponent } from './admin-modules/admin-parts-management/admin-group-management/admin-group-management.component';
import { AdminSubGroupManagementComponent } from './admin-modules/admin-parts-management/admin-sub-group-management/admin-sub-group-management.component';
import { AdminSubNodeManagementComponent } from './admin-modules/admin-parts-management/admin-sub-node-management/admin-sub-node-management.component';
import { AdminParameterManagementComponent } from './admin-modules/admin-parts-management/admin-parameter-management/admin-parameter-management.component';
import { AdminModelManagementComponent } from './admin-modules/admin-parts-management/admin-model-management/admin-model-management.component';
import { CmsSocialMediaLinksComponent } from './admin-modules/cms/cms-social-media-links/cms-social-media-links.component';
import { CmsUsefullLinksComponent } from './admin-modules/cms/cms-usefull-links/cms-usefull-links.component';
import { AdminAddSocialMediaLinksComponent } from './admin-modules/cms/cms-social-media-links/admin-add-social-media-links/admin-add-social-media-links.component';
import { AdminAddUsefullLinksComponent } from './admin-modules/cms/cms-usefull-links/admin-add-usefull-links/admin-add-usefull-links.component';
import { CmsNavLinksComponent } from './admin-modules/cms/cms-nav-links/cms-nav-links.component';
import { AdminAddNavLinksComponent } from './admin-modules/cms/cms-nav-links/admin-add-nav-links/admin-add-nav-links.component';
import { AdminCategoryRequestStatusComponent } from './admin-modules/admin-status-management/admin-category-request-status/admin-category-request-status.component';
import { AddAdminCategoryRequestStatusComponent } from './admin-modules/admin-status-management/admin-category-request-status/add-admin-category-request-status/add-admin-category-request-status.component';
import { AddAdminMakeOfferStatusComponent } from './admin-modules/admin-status-management/admin-make-offer-status/add-admin-make-offer-status/add-admin-make-offer-status.component';
import { AdminOrderStatusComponent } from './admin-modules/admin-status-management/admin-order-status/admin-order-status.component';
import { AddAdminOrderStatusComponent } from './admin-modules/admin-status-management/admin-order-status/add-admin-order-status/add-admin-order-status.component';
import { AdminPartsRequestStatusComponent } from './admin-modules/admin-status-management/admin-parts-request-status/admin-parts-request-status.component';
import { AddAdminPartsRequestStatusComponent } from './admin-modules/admin-status-management/admin-parts-request-status/add-admin-parts-request-status/add-admin-parts-request-status.component';
import { AdminPaymentStatusComponent } from './admin-modules/admin-status-management/admin-payment-status/admin-payment-status.component';
import { AddAdminPaymentStatusComponent } from './admin-modules/admin-status-management/admin-payment-status/add-admin-payment-status/add-admin-payment-status.component';
import { AdminQuoteStatusComponent } from './admin-modules/admin-status-management/admin-quote-status/admin-quote-status.component';
import { AddAdminQuoteStatusComponent } from './admin-modules/admin-status-management/admin-quote-status/add-admin-quote-status/add-admin-quote-status.component';
import { AdminRegistrationStatusComponent } from './admin-modules/admin-status-management/admin-registration-status/admin-registration-status.component';
import { AddAdminRegistrationStatusComponent } from './admin-modules/admin-status-management/admin-registration-status/add-admin-registration-status/add-admin-registration-status.component';
import { AdminShipmentStatusComponent } from './admin-modules/admin-status-management/admin-shipment-status/admin-shipment-status.component';
import { AddAdminShipmentStatusComponent } from './admin-modules/admin-status-management/admin-shipment-status/add-admin-shipment-status/add-admin-shipment-status.component';
import { AdminTaxCodesComponent } from './admin-modules/admin-tax-management/admin-tax-codes/admin-tax-codes.component';
import { AddAdminTaxCodesComponent } from './admin-modules/admin-tax-management/admin-tax-codes/add-admin-tax-codes/add-admin-tax-codes.component';
import { AdminJurisdictionComponent } from './admin-modules/admin-tax-management/admin-jurisdiction/admin-jurisdiction.component';
import { AddAdminJurisdictionComponent } from './admin-modules/admin-tax-management/admin-jurisdiction/add-admin-jurisdiction/add-admin-jurisdiction.component';
import { AdminPlaceOfSupplyComponent } from './admin-modules/admin-tax-management/admin-place-of-supply/admin-place-of-supply.component';
import { AddAdminPlaceOfSupplyComponent } from './admin-modules/admin-tax-management/admin-place-of-supply/add-admin-place-of-supply/add-admin-place-of-supply.component';
import { AdminVatDetailsComponent } from './admin-modules/admin-tax-management/admin-vat-details/admin-vat-details.component';
import { AddAdminVatDetailsComponent } from './admin-modules/admin-tax-management/admin-vat-details/add-admin-vat-details/add-admin-vat-details.component';
import { AdminMakeOfferStatusComponent } from './admin-modules/admin-status-management/admin-make-offer-status/admin-make-offer-status.component';
import { AdminAddCountryComponent } from './admin-modules/app-features/addcountry/admin-add-country/admin-add-country.component';
import { AdminAddCurrencyComponent } from './admin-modules/app-features/addcurrency/admin-add-currency/admin-add-currency.component';
import { AdminAddPayoutScheduleComponent } from './admin-modules/app-features/addpayout-schedule/admin-add-payout-schedule/admin-add-payout-schedule.component';
import { AdminAddProductTypeComponent } from './admin-modules/app-features/addproduct-type/admin-add-product-type/admin-add-product-type.component';
import { AdminAddReasonComponent } from './admin-modules/app-features/addreason/admin-add-reason/admin-add-reason.component';
import { AdminAddRoleComponent } from './admin-modules/app-features/addrole/admin-add-role/admin-add-role.component';
import { AdminAddStateComponent } from './admin-modules/app-features/addstate/admin-add-state/admin-add-state.component';
import { AdminAddSubscriptionPlanComponent } from './admin-modules/app-features/addsubscription-plan/admin-add-subscription-plan/admin-add-subscription-plan.component';
import { AdminAddTradingTypeComponent } from './admin-modules/app-features/addtrading-type/admin-add-trading-type/admin-add-trading-type.component';
import { AdminAddUserTypeComponent } from './admin-modules/app-features/adduser-type/admin-add-user-type/admin-add-user-type.component';
import { AdminTemplatesManagementComponent } from './admin-modules/admin-templates-management/admin-templates-management.component';
import { AdminSmsTemplateManagementComponent } from './admin-modules/admin-templates-management/admin-sms-template-management/admin-sms-template-management.component';
import { AdminAddSmsTemplatesComponent } from './admin-modules/admin-templates-management/admin-sms-template-management/admin-add-sms-templates/admin-add-sms-templates.component';
import { AdminEmailTemplateManagementComponent } from './admin-modules/admin-templates-management/admin-email-template-management/admin-email-template-management.component';
import { AdminAddEmailTemplatesComponent } from './admin-modules/admin-templates-management/admin-email-template-management/admin-add-email-templates/admin-add-email-templates.component';
import { AdminAddSubscriptionManagementComponent } from './admin-modules/admin-subscription-management/admin-add-subscription-management/admin-add-subscription-management.component';
import { AdminSubscriptionManagementComponent } from './admin-modules/admin-subscription-management/admin-subscription-management.component';
import { AdminProductManagementComponent } from './admin-modules/admin-product-management/admin-product-management.component';
import { AdminAddProductManagementComponent } from './admin-modules/admin-product-management/admin-add-product-management/admin-add-product-management.component';
import { AdminAddCommissionManagementComponent } from './admin-modules/admin-commission-management/admin-add-commission-management/admin-add-commission-management.component';
import { AdminCommissionManagementComponent } from './admin-modules/admin-commission-management/admin-commission-management.component';
import { AdminVinSearchHistoryManagementComponent } from './admin-modules/admin-vin-search-history-management/admin-vin-search-history-management.component';
import { AdminAddOrderComponent } from './admin-modules/admin-order-management/admin-add-order/admin-add-order.component';
import { AdminAddStaticPageComponent } from './admin-modules/admin-static-page-management/admin-add-static-page/admin-add-static-page.component';
import { AdminStaticPageManagementComponent } from './admin-modules/admin-static-page-management/admin-static-page-management.component';
import { AdminMakeOfferManagementComponent } from './admin-modules/admin-make-offer-management/admin-make-offer-management.component';
import { AdminAddMakeOfferComponent } from './admin-modules/admin-make-offer-management/admin-add-make-offer/admin-add-make-offer.component';
import { AdminAddPaymentComponent } from './admin-modules/admin-payment-management/admin-add-payment/admin-add-payment.component';
import { AdminPaymentManagementComponent } from './admin-modules/admin-payment-management/admin-payment-management.component';
import { AdminConfigurationDetailsManagementComponent } from './admin-modules/admin-templates-management/admin-configuration-details-management/admin-configuration-details-management.component';
import { AdminAddConfigurationDetailsComponent } from './admin-modules/admin-templates-management/admin-configuration-details-management/admin-add-configuration-details/admin-add-configuration-details.component';
import { CmsMobileSectionComponent } from './admin-modules/cms/cms-mobile-section/cms-mobile-section.component';
import { AdminAddMobileSectionComponent } from './admin-modules/cms/cms-mobile-section/admin-add-mobile-section/admin-add-mobile-section.component';


import { HashLocationStrategy, LocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    EcellsComponent,
    AddbuyersComponent,
    DashboardComponent,
    AddsellersComponent,
    AddsettingsComponent,
    AddintermedaitorComponent,
    AddcarsComponent,
    LandingComponent,
    NavigationComponent,
    HomeComponent,
    FooterComponent,
    ContactComponent,
    PricingComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    AddcityComponent,
    AddcountryComponent,
    AddcurrencyComponent,
    AddpayoutScheduleComponent,
    AddproductTypeComponent,
    AddreasonComponent,
    AddroleComponent,
    AddstateComponent,
    AddbuyerComponent,
    AddsellerComponent,
    AddintermediatorComponent,
    AddsubscriptionPlanComponent,
    AddtradingTypeComponent,
    AdduserTypeComponent,
    CmsInfoComponent,
    CmsBannerComponent,
    CmsFeaturesComponent,
    CmsHeroSectionComponent,
    CmsServicesComponent,
    CmsFooterComponent,
    AddUsersComponent,
    CmsBuyerSellerComponent,
    BuyerMainLayoutComponent,
    SellerMainLayoutComponent,
    BuyerDashboardComponent,
    SellerDashboardComponent,
    ViewOrdersComponent,
    ListOrdersComponent,
    ListProductsComponent,
    ViewProductComponent,
    ListTeamsComponent,
    ViewTeamComponent,
    OrderManagementComponent,
    ProductManagementComponent,
    TeamManagementComponent,
    BuyerHomeComponent,
    BuyerPagesComponent,
    BuyerNavigationComponent,
    BuyerOrderManagementComponent,
    BuyerListOrdersComponent,
    BuyerViewOrderComponent,
    AdminOrderManagementComponent,
    AdminViewOrderDetailsComponent,
    AdminListOrdersComponent,
    AdminAddBannerComponent,
    AdminAddFeaturesComponent,
    AdminAddServicesComponent,
    AdminAddsellerComponent,
    AdminAddbuyerComponent,
    AdminAddintermediatorComponent,
    AdminBrandManagementComponent,
    AdminCarManagementComponent,
    AdminGroupManagementComponent,
    AdminSubGroupManagementComponent,
    AdminSubNodeManagementComponent,
    AdminParameterManagementComponent,
    AdminModelManagementComponent,
    CmsSocialMediaLinksComponent,
    CmsUsefullLinksComponent,
    AdminAddSocialMediaLinksComponent,
    AdminAddUsefullLinksComponent,
    CmsNavLinksComponent,
    AdminAddNavLinksComponent,
    AdminCategoryRequestStatusComponent,
    AddAdminCategoryRequestStatusComponent,
    AddAdminMakeOfferStatusComponent,
    AdminOrderStatusComponent,
    AddAdminOrderStatusComponent,
    AdminPartsRequestStatusComponent,
    AddAdminPartsRequestStatusComponent,
    AdminPaymentStatusComponent,
    AddAdminPaymentStatusComponent,
    AdminQuoteStatusComponent,
    AddAdminQuoteStatusComponent,
    AdminRegistrationStatusComponent,
    AdminMakeOfferStatusComponent,
    AddAdminMakeOfferStatusComponent,
    AddAdminRegistrationStatusComponent,
    AdminShipmentStatusComponent,
    AddAdminShipmentStatusComponent,
    AdminTaxCodesComponent,
    AddAdminTaxCodesComponent,
    AdminJurisdictionComponent,
    AddAdminJurisdictionComponent,
    AdminPlaceOfSupplyComponent,
    AddAdminPlaceOfSupplyComponent,
    AdminVatDetailsComponent,
    AddAdminVatDetailsComponent,
    AdminAddCountryComponent,
    AdminAddCurrencyComponent,
    AdminAddPayoutScheduleComponent,
    AdminAddProductTypeComponent,
    AdminAddReasonComponent,
    AdminAddRoleComponent,
    AdminAddStateComponent,
    AdminAddSubscriptionPlanComponent,
    AdminAddTradingTypeComponent,
    AdminAddUserTypeComponent,
    AdminTemplatesManagementComponent,
    AdminSmsTemplateManagementComponent,
    AdminAddSmsTemplatesComponent,
    AdminEmailTemplateManagementComponent,
    AdminAddEmailTemplatesComponent,
    AdminAddSubscriptionManagementComponent,
    AdminSubscriptionManagementComponent,
    AdminProductManagementComponent,
    AdminAddProductManagementComponent,
    AdminAddCommissionManagementComponent,
    AdminCommissionManagementComponent,
    AdminVinSearchHistoryManagementComponent,
    AdminAddOrderComponent,
    AdminAddStaticPageComponent,
    AdminStaticPageManagementComponent,
    AdminMakeOfferManagementComponent,
    AdminAddMakeOfferComponent,
    AdminAddPaymentComponent,
    AdminPaymentManagementComponent,
    AdminConfigurationDetailsManagementComponent,
    AdminAddConfigurationDetailsComponent,
    CmsMobileSectionComponent,
    AdminAddMobileSectionComponent  
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule,
    NgSelectModule,
    AngularEditorModule,
    HttpClientModule,
    NgxPaginationModule,
    FilterPipeModule,
    AutocompleteLibModule,
    CarouselModule,
    DataTablesModule,
    FlexLayoutModule,
    ChartsModule,
    CommonModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
